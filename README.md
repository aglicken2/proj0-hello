# Proj0-Hello
-------------

By Anne Glickenhaus for CIS 322 in spring 2020. email: aglicken@uoregon.edu

This program runs "Hello World" using python. Program is run by calling "make run".
